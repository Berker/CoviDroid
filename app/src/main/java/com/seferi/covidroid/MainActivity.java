package com.seferi.covidroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    Button refreshBtn;
    TextView todayRecovered;
    TextView todayNewCases;
    TextView todayDeaths;

    TextView totalActive;
    TextView totalCases;
    TextView totalCritical;
    TextView totalRecovered;
    TextView totalDeaths;
    TextView totalPopulation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getGlobalData();
    }

    public void onRefreshBtnClick(View view) {
        refreshBtn = findViewById(R.id.btnRefresh);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGlobalData();
            }
        });
    }

    public void getGlobalData() {

        Gson gson = new Gson();

        String url = "https://disease.sh/v3/covid-19/all";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Post post = gson.fromJson(response, Post.class);
                todayRecovered = findViewById(R.id.TodayRecoveredNumbers);
                todayRecovered.setText(String.valueOf(post.getRecovered()));
                todayNewCases = findViewById(R.id.TodayNewCaseNumbers);
                todayNewCases.setText(String.valueOf(post.getTodayCases()));
                todayDeaths = findViewById(R.id.TodayDeathNumbers);
                todayDeaths.setText(String.valueOf(post.getTodayDeaths()));

                totalActive = findViewById(R.id.TotalActiveNumbers);
                totalActive.setText(String.valueOf(post.getActive()));
                totalCases = findViewById(R.id.TotalCaseNumbers);
                totalCases.setText(String.valueOf(post.getCases()));
                totalCritical = findViewById(R.id.TotalCriticalNumbers);
                totalCritical.setText(String.valueOf(post.getCritical()));
                totalRecovered = findViewById(R.id.TotalRecoveredNumbers);
                totalRecovered.setText(String.valueOf(post.getRecovered()));
                totalDeaths = findViewById(R.id.TotalDeathNumbers);
                totalDeaths.setText(String.valueOf(post.getDeaths()));
                totalPopulation = findViewById(R.id.TotalPopulation);
                totalPopulation.setText(String.valueOf(post.getPopulation()));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
        queue.start();
    }
}